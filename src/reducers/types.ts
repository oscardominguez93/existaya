import { UserActionsTypes } from './User/UserActions';

declare global {
  interface IUserReducerType {
    users: [IUser];
    user: IUser;
    count: number;
  }

  interface IUserActionsType {
    type: UserActionsTypes;
    value: any;
  }
}
