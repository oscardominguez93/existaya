import { Reducer } from 'redux';
import { UserActionsTypes } from './UserActions';

const initialState: IUserReducerType = {
  users: null,
  user: null,
  count: 1,
};

const reducer: Reducer<IUserReducerType, IUserActionsType> = (state = initialState, action) => {
  console.log('10 action >>> ', action);
  switch (action.type) {
    case UserActionsTypes.SET_USER_DATA:
      return {
        ...state,
        user: action.value,
      };
    case UserActionsTypes.SET_USERS_DATA:
      return {
        ...state,
        users: action.value,
      };
    default:
      return state;
  }
};

export default reducer;
