import { dispatch } from '@angular-redux/store';
import { ApiServices } from './../../app/services/api.service';
import { Injectable } from '@angular/core';
export enum UserActionsTypes {
  GET_USER_DATA = 'GET_USER_DATA',
  GET_USERS_DATA = 'GET_USERS_DATA',
  SET_USERS_DATA = 'SET_USERS_DATA',
  SET_USER_DATA = 'SET_USER_DATA',
}

@Injectable()
export class UserActions {
  ApiService: ApiServices;
  constructor(ApiService: ApiServices) {
    this.ApiService = ApiService;
  }
  @dispatch()
  getUsersData = () => {
    return async (dispatchUser, getState) => {
      const users: IRequestUsers = await this.ApiService.getUsers();
      dispatchUser(this.setUsersData(users.result));
    };
  }

  setUsersData = (value) => {
    return {
      type: UserActionsTypes.SET_USERS_DATA,
      value,
    };
  }

  setUserData = (value) => {
    return {
      type: UserActionsTypes.SET_USER_DATA,
      value,
    };
  }
}
