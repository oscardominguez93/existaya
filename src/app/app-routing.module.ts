import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaComponent } from './lista-component/lista-component.component';
import { PerfilComponent } from './perfil-component/perfil-component.component';

const routes: Routes = [
  { path: '', component: ListaComponent },
  { path: 'ListaComponent', component: ListaComponent },
  { path: 'perfil/:id', component: PerfilComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
