import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-perfil-component',
  templateUrl: './perfil-component.component.html',
  styleUrls: ['./perfil-component.css']
})
export class PerfilComponent implements OnInit {
  private id: number;
  private statistics = [
    {
      name: 'Proyectos',
      number: 1,
      status: 'Archivados',
      statusNumber: 0
    }, {
      name: 'Tareas',
      number: 2,
      status: 'Terminado',
      statusNumber: 0
    }, {
      name: 'Hitos',
      number: 0,
      status: 'Terminado',
      statusNumber: 0
    }
  ];
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = parseInt(params.id, 10);
    });
  }

}
