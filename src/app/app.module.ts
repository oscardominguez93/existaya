import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './Menu/Menu.component';
import { HttpClientModule } from '@angular/common/http';

// Material
import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

// Components
import { PerfilComponent } from './perfil-component/perfil-component.component';
import { ListaComponent } from './lista-component/lista-component.component';
import { TableComponent } from './components/table/table.component';

// Redux
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';
import { configureStorage } from './../redux/setupStore';
import { UserActions } from './../reducers/User/UserActions';

// Service
import { ApiServices } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    PerfilComponent,
    MenuComponent,
    TableComponent,
  ],
  imports: [
    MatSlideToggleModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatGridListModule,
    MatSliderModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    NgReduxModule,
  ],
  providers: [
    UserActions,
    ApiServices,
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(ngRedux: NgRedux<IReducerState>, private devTools: DevToolsExtension) {
    const store = configureStorage(devTools);
    ngRedux.provideStore(store);
  }
}
