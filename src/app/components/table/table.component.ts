import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() users: IUser[];



  displayedColumns: string[] = ['iconUser', 'iconProfile', 'iconEmail', 'name', 'lastName', 'website', 'dob', 'email', 'address', 'phone'];

  colorsBtn: string[] = [
    'primary',
    'accent',
    'warn'
  ];

  coloresSelected: string[] = [];

  constructor() { }

  ngOnInit() {

  }

  colorRandom(id) {
    const color = this.colorsBtn[Math.floor(Math.random() * this.colorsBtn.length)];
    return color;
  }

}
