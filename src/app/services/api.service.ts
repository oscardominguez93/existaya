import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { GLOBAL } from './global';
@Injectable({
  providedIn: 'root'
})
export class ApiServices {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) {

  }

  async getUsers() {
    const urlGetUsers = `${GLOBAL.url_api}users?_format=json&access-token=2sTTRZ41l-OXUyHqJQDmVQph7HYgT8A0Mw9X`;
    try {
      return await this.http.get<IRequestUsers>(urlGetUsers).toPromise();
    } catch (e) {
      throw e;
    }
  }
}
