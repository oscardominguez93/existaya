import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { UserActions } from './../../reducers/User/UserActions';
@Component({
  selector: 'app-lista-component',
  templateUrl: './lista-component.component.html',
  styleUrls: ['./lista-component.css']
})

export class ListaComponent implements OnInit {
  private actions: UserActions;
  constructor(actions: UserActions, private ngRedux: NgRedux<IReducerState>) {
    this.actions = actions;
  }

  ngOnInit() {
    this.getUsers();
  }

  async getUsers() {
    await this.actions.getUsersData();
  }

  infoUsers(): IUser[] {
    return this.ngRedux.getState().user.users || [];
  }
}
