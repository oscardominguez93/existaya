import { combineReducers } from 'redux';

import UserReducer from './../reducers/User/UserReducer';

export default combineReducers<IReducerState>({
  user: UserReducer,
});
