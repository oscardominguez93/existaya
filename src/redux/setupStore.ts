import { applyMiddleware, createStore, compose, AnyAction } from 'redux';
import combineReducer from './combineReducers';
import thunk from 'redux-thunk';
import { DevToolsExtension } from '@angular-redux/store';

export const configureStorage = (devTools?: DevToolsExtension, initialState?: IReducerState) => {
  const enhancer = compose(
    applyMiddleware(thunk),
    devTools.enhancer(),
  );
  const store = createStore<IReducerState, AnyAction, {}, {}>(
    combineReducer,
    initialState,
    enhancer,
  );
  return store;
};
