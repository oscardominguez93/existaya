interface IUser {
  id: string;
  name: string;
  lastName: string;
  webSite: string;
  dob: string;
  email: string;
  address: string;
  phone: string;
  position: string;
  proyects: number;
  tasks: number;
  milestone: number;
  permissions: [IPermissions];
}

interface IPermissions {
  name: string;
  state: boolean;
}

interface IReducerState {
  user: IUserReducerType;
}

